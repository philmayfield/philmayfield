import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import './PvGallery.css';

const root = 'http://philmayfield.com/img/pv/';

const images = [
  {
    originalTitle: 'Polar Booking Info',
    originalAlt: 'Polar Booking Info',
    original: `${root}polar1.png`,
    thumbnail: `${root}polar1_tn.png`
  },
  {
    originalTitle: 'Polar Booking Confirmation',
    originalAlt: 'Polar Booking Confirmation',
    original: `${root}polar2.png`,
    thumbnail: `${root}polar2_tn.png`
  },
  {
    originalTitle: 'Polar Pricing Summary',
    originalAlt: 'Polar Pricing Summary',
    original: `${root}polar4.png`,
    thumbnail: `${root}polar4_tn.png`
  },
  {
    originalTitle: 'Polar Commission Details',
    originalAlt: 'Polar Commission Details',
    original: `${root}polar5.png`,
    thumbnail: `${root}polar5_tn.png`
  }
];

const lightImages = [
  {
    originalTitle: 'Guest Search',
    originalAlt: 'Guest Search',
    original: `${root}guest-search.png`,
    thumbnail: `${root}guest-search_tn.png`
  },
  {
    originalTitle: 'Voyage Search',
    originalAlt: 'Voyage Search',
    original: `${root}voyage-search.png`,
    thumbnail: `${root}voyage-search_tn.png`
  },
  {
    originalTitle: 'Voyage Search Results',
    originalAlt: 'Voyage Search Results',
    original: `${root}voyage-search-results.png`,
    thumbnail: `${root}voyage-search-results_tn.png`
  },
  {
    originalTitle: 'Category Selection',
    originalAlt: 'Category Selection',
    original: `${root}category-selection.png`,
    thumbnail: `${root}category-selection_tn.png`
  },
  {
    originalTitle: 'Cabin Selection',
    originalAlt: 'Cabin Selection',
    original: `${root}category-selection-cabin-modal.png`,
    thumbnail: `${root}category-selection-cabin-modal_tn.png`
  },
  {
    originalTitle: 'Berthing',
    originalAlt: 'Berthing',
    original: `${root}berthing.png`,
    thumbnail: `${root}berthing_tn.png`
  },
  {
    originalTitle: 'Modify Booking',
    originalAlt: 'Modify Booking',
    original: `${root}modify.png`,
    thumbnail: `${root}modify_tn.png`
  },
  {
    originalTitle: 'Modify Details',
    originalAlt: 'Modify Details',
    original: `${root}modify-details.png`,
    thumbnail: `${root}modify-details_tn.png`
  }
];

const darkImages = [
  {
    originalTitle: 'Guest Search Dark',
    originalAlt: 'Guest Search Dark',
    original: `${root}guest-search-dark.png`,
    thumbnail: `${root}guest-search-dark_tn.png`
  },
  {
    originalTitle: 'Voyage Search Dark',
    originalAlt: 'Voyage Search Dark',
    original: `${root}voyage-search-dark.png`,
    thumbnail: `${root}voyage-search-dark_tn.png`
  },
  {
    originalTitle: 'Voyage Search Results Dark',
    originalAlt: 'Voyage Search Results Dark',
    original: `${root}voyage-search-results-dark.png`,
    thumbnail: `${root}voyage-search-results-dark_tn.png`
  },
  {
    originalTitle: 'Category Selection Dark',
    originalAlt: 'Category Selection Dark',
    original: `${root}category-selection-dark.png`,
    thumbnail: `${root}category-selection-dark_tn.png`
  },
  {
    originalTitle: 'Cabin Selection Dark',
    originalAlt: 'Cabin Selection Dark',
    original: `${root}category-selection-cabin-modal-dark.png`,
    thumbnail: `${root}category-selection-cabin-modal-dark_tn.png`
  },
  {
    originalTitle: 'Berthing Dark',
    originalAlt: 'Berthing Dark',
    original: `${root}berthing-dark.png`,
    thumbnail: `${root}berthing-dark_tn.png`
  },
  {
    originalTitle: 'Modify Booking Dark',
    originalAlt: 'Modify Booking Dark',
    original: `${root}modify-dark.png`,
    thumbnail: `${root}modify-dark_tn.png`
  },
  {
    originalTitle: 'Modify Details Dark',
    originalAlt: 'Modify Details Dark',
    original: `${root}modify-details-dark.png`,
    thumbnail: `${root}modify-details-dark_tn.png`
  }
];

class PvGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collectionItems: images.concat(lightImages),
      collectionTheme: 'Light'
    };

    this.changeTheme = this.changeTheme.bind(this);
  }

  changeTheme(e) {
    e.preventDefault();

    const theme = this.state.collectionTheme;

    this.setState({ collectionTheme: theme === 'Dark' ? 'Light' : 'Dark' });
    this.setState({
      collectionItems:
        theme === 'Dark'
          ? images.concat(lightImages)
          : images.concat(darkImages)
    });
  }

  render() {
    const { collectionTheme, collectionItems } = this.state;

    return (
      <div className="PvGallery text-center">
        <small>
          Currently showing the &ldquo;
          {collectionTheme}
          &rdquo; theme.{' '}
          <a href="#" role="button" onClick={this.changeTheme}>
            Switch to the {collectionTheme === 'Dark' ? 'Light' : 'Dark'} theme
          </a>
        </small>
        <ImageGallery items={collectionItems} showPlayButton={false} />
      </div>
    );
  }
}

export default PvGallery;
