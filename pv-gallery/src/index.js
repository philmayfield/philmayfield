import React from 'react';
import ReactDOM from 'react-dom';
import PvGallery from './PvGallery';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<PvGallery />, document.getElementById('pv-gallery'));
registerServiceWorker();
