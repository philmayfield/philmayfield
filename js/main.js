// set up config object for web-font-loader
var WebFontConfig = {
  google: {
    families: ["Anton", "Roboto"]
  },
  active: function() {
    new CircleType(document.getElementById("c1")).radius(500);
    new CircleType(document.getElementById("c2")).dir(-1).radius(600);
  }
};
