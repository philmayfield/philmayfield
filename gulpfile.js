const gulp = require("gulp");
const sass = require("gulp-sass");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const cssmin = require("gulp-cssmin");
const rename = require("gulp-rename");
const svgmin = require("gulp-svgmin");
const cheerio = require("gulp-cheerio");
const svgstore = require("gulp-svgstore");
const autoprefixer = require("gulp-autoprefixer");

// compile scss
gulp.task("scss", function() {
  return gulp
    .src("./scss/main.scss")
    .pipe(sass())
    .pipe(rename("main.min.css"))
    .pipe(autoprefixer({ browsers: ["last 2 version"] }))
    .pipe(cssmin())
    .pipe(gulp.dest("./build/css"));
});

// concat and uglify js
gulp.task("scripts", function() {
  return gulp
    .src([
      "./js/circletype.min.js",
      "./js/svg4everybody.min.js",
      "./js/plugins.js",
      "./js/main.js"
    ])
    .pipe(concat("main.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./build/js"));
});

// Create SVG sprite
gulp.task("svg-store", function() {
  return gulp
    .src("./svg/store/**/*")
    .pipe(svgmin())
    .pipe(
      cheerio({
        run: function($) {
          $("path").each(function() {
            if ($(this).attr("fill") == "none") {
              $(this).remove();
            }
          });
          // $('[fill]').removeAttr('fill');
        },
        parserOptions: { xmlMode: true }
      })
    )
    .pipe(svgstore())
    .pipe(rename("defs.svg"))
    .pipe(gulp.dest("./build/img"));
});

// Minify individual svgs
gulp.task("svg-solo", function() {
  return gulp
    .src("./svg/solo/**/*")
    .pipe(svgmin())
    .pipe(gulp.dest("./build/img"));
});

// Watch Files For Changes
gulp.task("watch", function() {
  gulp.watch("./js/**/*.js", ["scripts"]);
  gulp.watch("./scss/**/*.scss", ["scss"]);
  gulp.watch("./svg/**/*.svg", ["svg-store", "svg-solo"]);
});

// Default Task
gulp.task("default", ["scss", "scripts", "svg-store", "svg-solo", "watch"]);
